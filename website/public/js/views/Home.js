define(['text!templates/Home.html'], 
	function(template) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			initialize : function() {

			}

		});

});