define(['text!templates/LoginTemplate.html'], 
	function(template, loginTemplate) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			events : {
				'click #login-button' : 'processLogin'
			},

			initialize : function() {

			},

			onRender : function() {

			},

			processLogin : function() {
				
				var user = {
					firstName : "Joe",
					surname : "Bloggs",
					email : "joe.bloggs@gmail.com"
				};

				// Placeholder function for testing
				this.trigger('login', user);

			}

		});

});