define(['text!templates/AllCardsTemplate.html'], 
	function(template) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			events : {
				'click .filter-select' : 'toggleFilterPanel'
			},

			initialize : function(data) {

				this.viewModel = {

					category : data.ctg,

					icon : data.icon

				};

			},

			onRender : function() {

				// Set page title and icon
				$(this.el).find('.page-title h2').append('<i class="' + this.viewModel. icon + '"></i> ' + this.viewModel.category + ' Cards');

				// Add filter if all cards view
				if (this.viewModel.category == "All") {
					$(this.el).find('#all-cards-title-bar').append('<div class="filter-select uk-width-1-4">' + 
						'<i id="all-cards-filter" class="fa fa-filter fa-fw fa-2x"></i></div>');
				}

			},

			toggleFilterPanel : function(e) {

				$('#all-cards-filter').toggleClass('open');

				$('#filter-wrapper').slideToggle(400);
			},

		});

});