define(['text!templates/ContactPage.html'], 
	function(template) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			initialize : function() {

			}

		});

});