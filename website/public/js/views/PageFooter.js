define(['text!templates/PageFooterTemplate.html'], 
	function(template) {
	
		return Backbone.Marionette.ItemView.extend({

			template : template,

			initialize : function() {

			}

		});

});