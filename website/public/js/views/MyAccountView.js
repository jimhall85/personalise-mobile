define(['text!templates/MyAccountTemplate.html'], 
	function(template, loginTemplate) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			events : {
				
			},

			initialize : function(user) {

				this.viewModel = {

					user : user

				};

			},

			onRender : function() {

			}

		});

});