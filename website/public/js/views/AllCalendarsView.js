define(['text!templates/AllCalendarsTemplate.html'], 
	function(template) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			events : {
				'click .filter-select' : 'toggleFilterPanel'
			},

			initialize : function() {

			},

			toggleFilterPanel : function(e) {

				$('#all-calendars-filter').toggleClass('open');

				$('#filter-wrapper').slideToggle(400);
			}

		});

});