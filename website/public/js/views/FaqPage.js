define(['text!templates/FaqTemplate.html'], 
	function(template) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			initialize : function() {

			}

		});

});