define(['text!templates/DeliveryTemplate.html'], 
	function(template) {

		return Backbone.Marionette.ItemView.extend({

			template : template,

			initialize : function() {

			}

		});

});