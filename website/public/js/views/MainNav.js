define(['text!templates/MainNavTemplate.html'], 
	function(template) {
	
		return Backbone.Marionette.ItemView.extend({

			template : template,

			events : {

				"click #user-icon, #search-icon" : "toggleNavBars",
				"click #off-canvas-wrapper a" : "goTo",
				"click #nav-title" : "goToHome",
				"click #account-link" : "goToMyAccount"
			},

			initialize : function(options) {

				this.viewModel = {

					user : options.user

				};

			},

			toggleNavBars : function(e) {

				var target = e.currentTarget.attributes["data-toggle"].value;

				//sort this out like 

				if (target == '#search-bar' && $('#user-bar:visible').length > 0) {

					$('#user-bar').slideToggle(400, function() {

						$(target).slideToggle(400);

					});

				} else if (target == '#user-bar' && $('#search-bar:visible').length > 0) {

					$('#search-bar').slideToggle(400, function() {

						$(target).slideToggle(400);

					});

				} else {

					$(target).slideToggle(400);

				}

			},

			onCanvas : function() {

				$.UIkit.offcanvas.offcanvas.hide();

			},

			goTo : function(e) {

				if (e.currentTarget.id != "link-desktop") {

					e.preventDefault();

					// Get id and category
					var to = e.currentTarget.attributes["href"].value;
					var ctg = e.currentTarget.attributes["data-category"].value;

					// Get icon
					var iconClass = $(e.currentTarget).find('i').attr('class');

					// pass object for view init
					var data = {
						ctg : ctg,
						icon : iconClass
					};

					this.trigger(to, data);
					this.onCanvas();

				}

			},

			goToHome : function(e) {

				this.trigger("switchHome");

			},

			goToMyAccount : function(e) {

				this.trigger("switchMyAccount", this.viewModel.user);
				$('#user-bar').slideToggle(400);
			}

		});

});