define(['views/MainNav', 'views/PageFooter', 'views/Home', 'views/ContactPage', 'views/FaqPage', 'views/DeliveryCharges', 'views/AllCardsView', 'views/AllCalendarsView', 'views/MyAccountView', 'views/LoginView'],
	function(MainNav, PageFooter, Home, ContactPage, FaqPage, DeliveryCharges, AllCardsView, AllCalendarsView, MyAccountView, LoginView) {

		var App = new Backbone.Marionette.Application();

		App.addInitializer(function(options) {

			var mainNav = new MainNav(options);
			App.mainnav.show(mainNav);
			mainNav.on({

				'switchHome' : function() {
					App.content.show(home);
				},

				'switchContact' : function() {
					App.content.show(contactPage);
				},

				'switchFaq' : function() {
					App.content.show(faqPage);
				},

				'switchDelivery' : function() {
					App.content.show(deliveryCharges);
				},

				'switchAllCards' : function(data) {
					var allCardsView = new AllCardsView(data);
					App.content.show(allCardsView);

				},

				'switchAllCalendars' : function() {
					App.content.show(allCalendarsView);
				},

				'switchMyAccount' : function(user) {

					if (!user) {
						App.content.show(loginView);					
					} else {
						var myAccount = new MyAccountView(user);
						App.content.show(myAccount);
					}
				}
			});

			var pageFooter = new PageFooter();
			App.footer.show(pageFooter);

			var home = new Home();
			App.content.show(home);

			var loginView = new LoginView();
			loginView.on({

				'login' : function(creds) {

					options.user = creds;

					var myAccount = new MyAccountView(options.user);
					App.content.show(myAccount);
				}
			})

			var contactPage = new ContactPage();
			var faqPage = new FaqPage();
			var deliveryCharges = new DeliveryCharges();
			var allCalendarsView = new AllCalendarsView();
		
		});

		App.addRegions({

			mainnav : "#main-nav",
			content : "#page-content",
			footer : "#page-footer"

		});

		return App;

});